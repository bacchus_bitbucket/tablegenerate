<?php
arr();
function arr()
{
    $a = array(
        array('text' => 'Текст красного цвета'
        , 'cells' => '1,2,4,5'
        , 'align' => 'center'
        , 'valign' => 'center'
        , 'color' => 'FF0000'
        , 'bgcolor' => '0000FF'),

        array('text' => 'Текст серого цвета'
        , 'cells' => '8,9'
        , 'align' => 'center'
        , 'valign' => 'center'
        , 'color' => 'grey'
        , 'bgcolor' => 'orange'),
    );
    mas($a);
}

function mas($arr)
{
   $strStartTableTag = '<table cellpadding=\"100\" width=\"100%\" border=\"1\"> <tr>';
   $strEndTableTag = ' </tr></table></body>';
   $strStyle =
        '<style>table {
            width: 600px;
            height:600px;
            border-collapse: collapse;
        }
        td {
            width: 200px;
            height:200px;
            border: 1px solid black;
    } ';
    
    $str = '';
    foreach ($arr as $key => $value) {
        $str .= $value['cells'] . ',';
    }
    $str = substr($str, 0, -1);
    $str = explode(',', $str);
    $starCells = array(
        array(1, 2, 3),
        array(4, 5, 6),
        array(7, 8, 9)
    );
    for ($i = 0; $i < 3; $i++) {
        for ($j = 0; $j < 3; $j++) {
            //  $tmp = $a[$i][$j];
            if (!in_array($starCells[$i][$j], $str)) {
                $starCells[$i][$j] = 0;
            }
        }
    }
    $existsCells = array(array()); // уже отображенные ячейки

    for ($countStyles = count($arr) - 1; $countStyles >= 0; $countStyles--) {

        $a = array(
            array(1, 2, 3),
            array(4, 5, 6),
            array(7, 8, 9)
        );

        $cells = explode(',', $arr[$countStyles]['cells']);        //ячейки, принадлежащие данному класу
        for ($ii = 0; $ii < count($cells); $ii++) {
            $cells[$ii] = (int)$cells[$ii];
        }
        
        $txt = $arr[$countStyles]['text'];
        $align = $arr[$countStyles]['align'];
        $valign = $arr[$countStyles]['valign'];
        $color = $arr[$countStyles]['color'];
        $bgcolor = $arr[$countStyles]['bgcolor'];

        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 3; $j++) {
                //  $tmp = $a[$i][$j];
                if (!in_array($a[$i][$j], $cells)) {
                    $a[$i][$j] = 0;
                }
            }
        }
        $iStart = 0;//  верхняя левая ячейка,
        $jStart = 0;// входящая в класс
        $iEnd = 0;//  правая нижняя ячейка,
        $jEnd = 0;// входящая в класс
        $colspan = 1;
        $rowspan = 1;
        $flag = false;
        for ($i = 2; $i >= 0; $i--) {
            for ($j = 2; $j >= 0; $j--) {
                if ($a[$i][$j] != 0) {
                    // еще не найдено начало обрабатываемой области
                    if (!$flag) {
                        $iStart = $i;
                        $jStart = $j;
                        $iEnd = $i;
                        $jEnd = $j;
                        while ($iStart - 1 >= 0 && $a[$iStart - 1][$jStart] != 0) {
                            $iStart--;
                        }
                        while ($jStart - 1 >= 0 && $a[$iStart][$jStart - 1] != 0) {
                            $jStart--;
                        }
                        $flag = true;   // найдено начало обрабатываемой области
                    }
                    if ($flag &&
                        $i == $iStart &&
                        $j == $jStart
                    ) {
                        $flag = false;

                        if ($iEnd > $iStart) {
                            $rowspan = ($iEnd - $iStart) + 1;
                        }
                        if ($jEnd > $jStart) {
                            $colspan = ($jEnd - $jStart) + 1;
                        }
                        $strtag = '<td class = class' . $countStyles . ' rowspan=' .
                            $rowspan . ' colspan=' . $colspan . '>' . $txt . '</td> ';
                        $strEndTableTag = $strtag . $strEndTableTag;
                        if (($starCells[$i][$j - 1] != 0) && (!in_array($a[$i][$j - 1], $cells))) {
                            if (($j > 1) && (!in_array($a[$i][$j - 2], $starCells))) {
                            } else {

                                $strEndTableTag = '</tr><tr>' . $strEndTableTag;
                            }
                        } else
                            if ($j == 0) {
                                $strEndTableTag = '</tr><tr>' . $strEndTableTag;
                            }
                    }
                    //$tmp = array($i, $j);
                    if (!in_array(array($i, $j), $existsCells)) {
                        array_unshift($existsCells, array($i, $j));
                    }
                } //не  принадлежит данному класу
                else {
                   // небыло обработано и не принадлежит никакому другому класу
                    if (!in_array(array($i, $j), $existsCells) && $starCells[$i][$j] == 0) {
                        $strtag = '<td></td> ';
                        $strEndTableTag = $strtag . $strEndTableTag;

                        if ($i != 0) {
                            if ($j == 0) {
                                {
                                    $strEndTableTag = '</tr><tr>' . $strEndTableTag;
                                }
                            } else {
                                if ($j > 1) {
                                    if (($starCells[$i][$j - 2] != 0)) {
                                        $strEndTableTag = '</tr><tr>' . $strEndTableTag;
                                    }
                                } else
                                    if ($starCells[$i][$j - 1] != 0) {
                                        $strEndTableTag = '</tr><tr>' . $strEndTableTag;
                                    }
                            }
                        }
                        array_unshift($existsCells, array($i, $j));
                    }
                }
            }
        }
    $strStyle .= '    .class' . $countStyles . '{
    text-align:' . $align . ';
    valign:' . $valign . ';
    color:' . $color . ';
    background-color:' . $bgcolor . ';
     }';

    }
    $strStyle .= '</style>';
    $strResult = '<head>    <meta charset=\"UTF-8\"><title>Title</title></head><body>';
    $strResult .= $strStyle . $strStartTableTag . $strEndTableTag;
    echo '<pre>',$strResult, '<pre>';
}

?>